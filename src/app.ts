import {Component, View, provide, bootstrap, CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/angular2';
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router';
import {Art} from './article';
import {HomeInfo} from './homeInfo';
import {RemoteService} from './remoteService';
import { status, json } from './fetch'

@Component({
    selector: 'app'
})
@View({
    templateUrl: `template.html`,
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, ROUTER_DIRECTIVES]
})
@RouteConfig([
    { path: '/',  as: 'Home', component: HomeInfo },
    { path: '/Article/:id', as: 'Article', component: Art}
])
export class App {
    list:Object[] = [];
    remoteService:RemoteService;
    router: Router;

    constructor(remoteService:RemoteService, router: Router) {
        this.router = router;
        this.remoteService = remoteService;
        this.list = remoteService.getLinks();
    }


    vm: Object = {};
    searchByTitle(input, event){
        var id = this.remoteService.getByTitle(input).then(status)
            .then(json)
            .then((response) => {
                var articles = response._embedded.articles;
                if(articles.length>0){
                    var id = this.remoteService.extractDataFromArticleURL(articles[0]._links.self.href);
                    this.router.navigate(['./Article', {id: id}]);
                }
            });
    }
}
