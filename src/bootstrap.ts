import {provide, bootstrap, bind} from 'angular2/angular2';
import {ROUTER_PROVIDERS, HashLocationStrategy, LocationStrategy} from 'angular2/router';
import {App} from './app';
import {RemoteService} from './remoteService';

bootstrap(App, [
    ROUTER_PROVIDERS,
    RemoteService,
    provide(LocationStrategy, {useClass: HashLocationStrategy})
]);