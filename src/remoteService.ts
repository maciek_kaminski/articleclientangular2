import { status, json } from './fetch'
export class RemoteService {
    url: string;
    constructor() {
        this.url = 'http://localhost:8080/articles/';
    }

    getLinks() {
        var list: Object[] = [];

        var all = window.fetch(this.url).then(status)
            .then(json)
            .then((response) => {
                var articles = response._embedded.articles;
                for (var v in articles)
                {
                    list.unshift(this.newItem(articles[v].title, this.extractDataFromArticleURL(articles[v]._links.self.href)))
                }
            });


        return list;
    }

    getByTitle(title:string) {
        return window.fetch(this.url+'search/findByTitleLike?title='+title);
    }

    newItem(title:string, id:string) {
        return {
            'name': title,
            'id': id,
        };
    }


    getArticle(nr:number){
        return window.fetch(this.url + '/' + nr);
    }

    extractDataFromArticleURL(url:string){
        return url.replace(this.url, "");
    }
}