import {
    Component,
    View,
    CORE_DIRECTIVES,
    FORM_DIRECTIVES
} from 'angular2/angular2';
import { RouterLink, RouteParams } from 'angular2/router';
import {RemoteService} from './remoteService.ts';
import { status, json } from './fetch.ts'


@Component({
    selector: 'div[art]',
    injectables: [RemoteService]
})
@View({
    templateUrl: `src/article.html`,
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class Art {
    list: Object[] = [];
    title: string;
    text: string;
    constructor(remoteService:RemoteService, routeParam: RouteParams) {
        var id:number;
        id = routeParam.get("id");

        remoteService.getArticle(id).then(status)
            .then(json)
            .then((response) => {
                this.title = response.title;
                this.text = response.text;
            })

    }
}
